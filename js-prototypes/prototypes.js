/**
 * Implement required functions and run with:
 *
 * node prototypes.js
 */

console.info("Initializing prototypes.");

/**
 * Generic book
 * :param name - String
 * :param price - Number
 * :param currency - String
 */
function Product(name, price, currency) {
    this.name = name;
    this.price = price;
    this.currency = currency || "$";
}

Product.prototype.prettyPrice = function() {
    return this.currency + this.price;
};

/**
 * Physical book
 * :param name - String
 * :param price - Number
 * :param nPages - Number (int)
 */
function Book(name, price, nPages) {
    Product.call(this, name, price);
    this.nPages = nPages;
}
Book.prototype = new Product();

/**
 * Return true if the book is longer than 100 pages.
 */
Book.prototype.isLong = function() {
    // TODO implement
    return false;
};

/**
 * Digital book
 * :param name - String
 * :param price - Number
 * :param nPages - Number (int)
 * :param fileSize - Number (int) - in bytes
 */
function EBook(name, price, nPages, fileSize) {
    Book.call(this, name, price, nPages);
    this.fileSize = fileSize;
}
EBook.prototype = new Book();

/**
 * Return true if the file is bigger than 8MB.
 */
EBook.prototype.isHeavy = function() {
    // TODO implement
    return false;
};

/**
 * Just a pen.
 * :param name - String
 * :param price - Number
 */
function Pen(name, price) {
    Product.call(this, name, price);
}
Pen.prototype = new Product();

/**
 * Heterogeneous list of products.
 * :param products - Array
 */
function ProductsList(products) {
    this.products = products || [];
}

/**
 * Return the total price.
 */
ProductsList.prototype.getTotalPrice = function() {
    // TODO implement
    return 0;
};

/**
 * Return a list of pens.
 */
ProductsList.prototype.getPens = function() {
    // TODO implement
    return [];
};

/**
 * Return a list of books (and e-books).
 */
ProductsList.prototype.getBooks = function() {
    // TODO implement
    return [];
};

/**
 * Return a list of e-books.
 */
ProductsList.prototype.getEBooks = function() {
    // TODO implement
    return [];
};

/**
 * Return a list of short books (and e-books).
 */

ProductsList.prototype.getShortBooks = function() {
    // TODO implement
    return [];
};

/**
 * Return a list of heavy e-books.
 */

ProductsList.prototype.getHeavyEBooks = function() {
    // TODO implement
    return [];
};


// real data

console.info("Initializing objects.");

var mylist = new ProductsList([
    new Book("The Little Prince", 10, 70),
    new Book("1984", 15, 300),
    new EBook("The hard thing about hard things", 14, 250, 15728640),  // 15MB
    new EBook("The Elegance of the Hedgehog", 13, 160, 5242880),  // 5MB
    new EBook("Amazon Web Services in Action", 15, 400, 10485760),  // 10MB
    new Pen("Red Pen", 2),
    new Pen("Blue Pen", 1),
]);


// tests

console.info("Executing tests.");

if (mylist.getPens().length !== 2) {
    console.error("Wrong # of Pens:", mylist.getPens().length);
}

if (mylist.getBooks().length !== 5) {
    console.error("Wrong # of Books:", mylist.getBooks().length);
}

if (mylist.getEBooks().length !== 3) {
    console.error("Wrong # of Ebooks:", mylist.getEBooks().length);
}

if (mylist.getShortBooks().length !== 1) {
    console.error("Wrong # of short Books:", mylist.getShortBooks().length);
}

if (mylist.getHeavyEBooks().length !== 2) {
    console.error("Wrong # of heavy E-Books:", mylist.getHeavyEBooks().length);
}

if (mylist.getTotalPrice() !== 70) {
    console.error("Wrong total price:", mylist.getTotalPrice());
}

console.info("Done.");