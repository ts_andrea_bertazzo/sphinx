Please improve the slow controller by adding a memoize cache into the python flask application and, 
in addition, after the first request the client should only use local cache instead of calling the 
server again.

* wget https://bootstrap.pypa.io/get-pip.py
* python get-pip.py