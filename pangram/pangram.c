#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>
#include <ctype.h>


bool _pangram[26];


void AddChar(char* l)
{
	if(((int)*l) == 32) 
		return;
	
	int _temp = ((int)*l) - 65;
	_pangram[_temp] = true;
	return;
}

int IsPangram()
{
	for(int i = 0; i<26; i++)
		if(!_pangram[i])
			return 0;

	return 1;
}

/*
 * Complete the function below.
 */
int isPangram(char* n) {
	/* Write you solution here */
	// To Upper & Check
	while(*n != '\0')
	{
		*n = toupper(*n);
		AddChar(n);			
		n++;
	}

	return IsPangram();
}





int main() {
    int res;
    char* _n;
    _n = (char *)malloc(10240 * sizeof(char));
    scanf("\n%[^\n]",_n);
    res = isPangram(_n);
    printf("%d\n", res);
    
    return 0;
}
